Ext.define('Highcharts.store.AssetData', {
    extend : 'Ext.data.Store',
    autoLoad : false,
    config: {
        model: 'Highcharts.model.GenericGraphData',
        data:[{ 
            'category':'Cash',
            'value':14.40
        }, {
            'category':'Fixed Income',
            'value':73.39,
        }, {
            'category':'Equities',
            'value':7.67
        },{
            'category':'Structured prods.',
            'value':-5.20
        },{
            'category':'Hedge Funds',
            'value':-20.30
        },{
            'category':'Others',
            'value':4.80
        },
        ]
    }
});
