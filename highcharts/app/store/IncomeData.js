Ext.define('Highcharts.store.IncomeData', {
    extend : 'Ext.data.Store',
    autoLoad : false,
    config: {
        model: 'Highcharts.model.GenericGraphData',
        data:[{ 
            'category':'JPY',
            'value':28.9
        }, {
            'category':'GBP',
            'value':34.5,
        }, {
            'category':'CHF',
            'value':51.6
        },{
            'category':'Others',
            'value':11.13
        },{
            'category':'USD',
            'value':8.26
        },{
            'category':'EUR',
            'value':71.28
        },
        ]
    }
});
