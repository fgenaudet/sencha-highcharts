Ext.define('Highcharts.store.CurrencyData', {
    extend : 'Ext.data.Store',
    autoLoad : false,
    config: {
        model: 'Highcharts.model.GenericGraphData',
        data:[{ 
            'category':'JPY',
            'value':0
        }, {
            'category':'GBP',
            'value':1.90,
        }, {
            'category':'CHF',
            'value':5.04
        },{
            'category':'Others',
            'value':11.13
        },{
            'category':'USD',
            'value':15
        },{
            'category':'EUR',
            'value':50.30
        },
        ]
    }
});
