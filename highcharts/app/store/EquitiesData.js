Ext.define('Highcharts.store.EquitiesData', {
    extend : 'Ext.data.Store',
    autoLoad : false,
    config: {
        model: 'Highcharts.model.GenericGraphData',
        data:[{ 
            'category':'JPY',
            'value':71.59
        }, {
            'category':'GBP',
            'value':29.65,
        }, {
            'category':'CHF',
            'value':15.68
        },{
            'category':'Others',
            'value':97.56
        },{
            'category':'USD',
            'value':23.58
        },{
            'category':'EUR',
            'value': 1.25
        },
        ]
    }
});
