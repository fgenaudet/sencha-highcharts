Ext.define('Highcharts.model.GenericGraphData', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['category', 'value']
    }
});
