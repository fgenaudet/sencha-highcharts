var buildGradient = function (color) {
    var gradient = {
        linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken                    
                ]
            };
        return gradient;
};
var createStore = function(category) {
    var store = 'Highcharts.store.';
    if ('Cash' === category) {
        store += 'CurrencyData';
    } else if('Equities' === category) {
        store += 'EquitiesData';
    } else {
        store += 'IncomeData';
    }
    return Ext.create(store, {
           model: 'Highcharts.model.GenericGraphData'
    });
};
var defaultGraph = new Ext.create('Chart.ux.Highcharts',
{
                xtype: 'highchart',
                style: {
                    margin: '5px'
                },
                series : [{
                    dataIndex : 'value'
                }],
                store: Ext.create('Highcharts.store.CurrencyData', {
                    model: 'Highcharts.model.GenericGraphData'
                }),
                xField : 'category',
                chartConfig : {
                    colors: [
                    buildGradient('rgb(2,149,201)')
                    ],
                    chart : {
                        type: 'bar',
                        backgroundColor: {
                            radialGradient: { cx:"0", cy:"-37%", r:"135%"  },
                            stops: [
                            [0, '#353535'],
                            [0.8, '#353535'],
                            [0.8, '#2e2e2e'],
                            [1, '#2e2e2e']
                            ]
                        },
                        style: {
                            fontFamily: 'sans-serif'
                        }
                    },
                    title: {
                        text: 'CURRENCY BREAKDOWN',
                        style : {color: '#FFF', fontWeight: 'bold'},
                        align: 'left',
                        margin: 30
                    },
                    subtitle: {
                        text: 'Cash',
                        style : {color: 'darkgrey', fontWeight: 'bold', fontSize: '1.5rem'},
                        align: 'left',
                        margin: 30
                    },
                    yAxis: {
                        title: {
                            text: 'Million',
                            style: {
                                color: '#7c7c7c'
                            }

                        },

                        minorTickColor: 'transparent',
                        gridLineWidth: '1px',
                        gridLineColor: '#4f4f4f',

                        labels: {
                            formatter: function(){
                                return this.value;
                            },
                            style: {
                                color: '#7c7c7c'
                            },
                            overflow: 'justify'
                        },
                        min: 0,
                        plotLines: [{ 
                            color: '#989898',
                            width: 3,
                            value: 0
                        }]
                    },
                    xAxis : {
                        labels: {
                            step: 1,
                            useHTML: true,
                            formatter: function() {
                                return '<div class="xLabel">'+this.value+'</div>'
                            }
                        },
                        /*title : {
                            align: 'high',
                            offset: 35,
                            text: 'Cash',
                            rotation: 0,
                        }*/
                    },
                    plotOptions: {
                        series: {
                            borderColor: 'transparent'            
                        },

                        bar: {
                            allowPointSelect: true,
                            pointWidth: 35,
                            dataLabels: {
                                enabled: true,
                                style: {
                                    color: '#FFF',
                                    fontSize: 15      
                                },
                                useHTML: true,
                                formatter: function(){
                                    return "<i>"+this.point.y+"%</i>";
                                } 
                            }
                        },
                    },
                    legend: {
                        enabled: false
                    },

                    credits: {
                        enabled: false
                    },
                }
            }
);
Ext.define('Highcharts.view.Graph', {
    extend: 'Ext.Container',
    config : {
        layout: 'hbox',
        itemId: 'container',
        items: [
            {
                xtype: 'highchart',
                style: {
                    margin: '5px'
                },
                series : [{
                    dataIndex : 'value',
                    listeners:{
                             pointclick:function(serie,point,record,event){
                                 defaultGraph.setSubTitle(point.category);
                                 defaultGraph.bindStore(createStore(point.category));
                                 defaultGraph.draw();
                             }
                    },
                }],
                store: Ext.create('Highcharts.store.AssetData', {
                    model: 'Highcharts.model.GenericGraphData'
                }),
                xField : 'category',
                chartConfig : {
                    colors: [
                        buildGradient('rgb(0,136,1)'),
                        buildGradient('rgb(194,44,144)'),
                        buildGradient('rgb(113,56,135)'),
                        buildGradient('rgb(238,223,98)'),
                        buildGradient('rgb(174,197,215)'),
                        buildGradient('rgb(165,164,196)')
                    ],
                    chart: {
                        type: 'bar',
                        backgroundColor: {
                            radialGradient: { cx:"0", cy:"-37%", r:"135%"  },
                            stops: [
                            [0, '#353535'],
                            [0.8, '#353535'],
                            [0.8, '#2e2e2e'],
                            [1, '#2e2e2e']
                            ]
                        },
                        style: {
                            fontFamily: 'sans-serif'
                        },
                    },    

                    title: {
                        text: 'ASSET ALLOCATION',
                        style : {color: '#FFF', fontWeight: 'bold'},
                        align: 'left',
                        margin: 30
                    },

                    xAxis: [{
                        labels: {
                            step: 1,
                            useHTML: true,
                            formatter: function() {
                                return '<div class="xLabel-asset">'+this.value+'</div>'
                            }
                        },


                        plotLines: [{ 
                            color: '#989898',
                            width: 2,
                            value: 5.5
                        }]
                    }],
                    yAxis: {
                        title: {
                            text: 'Million',
                            style: {
                                color: '#7c7c7c'
                            }

                        },

                        minorTickColor: 'transparent',
                        gridLineWidth: '1px',
                        gridLineColor: '#4f4f4f',

                        labels: {
                            formatter: function(){
                                return this.value;
                            },
                            style: {
                                color: '#7c7c7c'
                            },
                            overflow: 'justify'
                        },
                        /*min: -12,
                        max: 20,*/
                        plotLines: [{ 
                            color: '#989898',
                            width: 3,
                            value: 0
                        }]
                    },

                    plotOptions: {
                        series: {
                            borderColor: 'transparent',
                            colorByPoint: true
                        },

                        bar: {
                            allowPointSelect: true,
                            pointWidth: 30,
                            dataLabels: {
                                enabled: true,
                                style: {
                                    color: '#FFF',
                                    fontSize: 15     
                                },
                                useHTML: true,
                                formatter: function(){
                                    return "<i>"+this.point.y+"%</i>";
                                } 
                            }
                        },
                    },
                    legend: {
                        enabled: false
                    },

                    credits: {
                        enabled: false
                    },
                }
            }, 
                defaultGraph
            ] 
    },
    
});
